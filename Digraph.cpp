/*--- Digraph.cpp ---------------------------------------------------------
 Implementation of the Digraph class
 -------------------------------------------------------------------------*/

#include "Digraph.h"
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
using namespace std;

template <typename T>
string myItoa(T object)
{
	stringstream sst;
	sst << object;
	return sst.str();
}

//--- Definition of data()
string Digraph::getData(int k) const {
  return Vertices[k].data;
}



void Digraph::InsertNode(string st) {
   Vertices.push_back(Vertex(st));
}

void Digraph::InsertEdge(int from, int to) {
  if (from < 0 || to < 0) {
    cout << "Warning in InsertEdge: invalid node index "
        << from << ", " << to << endl;
    return;
  }
	Vertices[from].adjacencyList.push_back(to);

	//for a non directed graph
	Vertices[to].adjacencyList.push_back(from);
}

//--- Definition of read()
void Digraph::read(ifstream & inStream) {
  //Digraph::VertexInfo vi;
  int n,           // number of vertices adjacent to some vertex
      vertex;      // the number of a vertex
  string st;

  int ctr = 0;
  for (;;) {

    inStream >> st;
    if (inStream.eof()) break;
    InsertNode(st);

    inStream >> n;

    for (int i = 1; i <= n; i++) {
      inStream >> vertex;
      InsertEdge(ctr, vertex);
    }
    ctr++;
  }

}

void Digraph::read_matrix(ifstream & inStream )
{
	int amount, vertices, dummy1, dummy2;
	string st_node1, st_node2;
	//string st = "Hola";

	inStream >> amount;

	inStream >> dummy1;

	inStream >> vertices;

	for(int i = 0; i < amount; i++)
	{
		st_node1 = myItoa(i);
		InsertNode(st_node1);
	}


	while(!inStream.eof())
	{
		inStream >> dummy1 >> dummy2;

		InsertEdge(dummy1, dummy2);
	}
}

//--- Definition of display()
void Digraph::display(ostream & out)
{
  out << "Adjacency-List Representation: \n";
  for (int i = 0; i < Vertices.size(); i++) {
    out << i << ": " <<  Vertices[i].data << "--";

    for (vector<int>::iterator
          it = Vertices[i].adjacencyList.begin();
          it != Vertices[i].adjacencyList.end(); it++)
      out << *it << "  ";

    out << endl;
  }
}


void Digraph::depthFirstSearch(int start, vector<bool> & unvisited)
{
  // Add statements here to process Vertices[start].data
  cout << Vertices[start].data << endl;

  unvisited[start] = false;
  // Traverse its adjacency list, performing depth-first
  // searches from each unvisited vertex in it.
  for (vector<int>::iterator
         it = Vertices[start].adjacencyList.begin();
         it != Vertices[start].adjacencyList.end(); it++)
    // check if current vertex has been visited
    if (unvisited[*it])
      // start DFS from new node
      depthFirstSearch(*it, unvisited);
}

//-- Definitions of depthFirstSearch() and depthFirstSearchAux()
inline void Digraph::depthFirstSearch(int start) {
  vector<bool> unvisited(Vertices.size(), true);
  depthFirstSearch(start, unvisited);
}


//--- Definition of shortestPath()
vector<int> Digraph::shortestPath(int start, int destination)
{
  int n = Vertices.size(); // number of vertices (#ed from 1)
  vector<int> distLabel(n,-1),     // distance labels for vertices, all
                                   // marked as unvisited (-1)
              predLabel(n);        // predecessor labels for vertices
  // Perform breadth first search from start to find destination,
  // labeling vertices with distances from start as we go.
  distLabel[start] = 0;
  int distance = 0,                // distance from start vertex
      vertex;                      // a vertex
  queue<int> vertexQueue;          // queue of vertices

  vertexQueue.push(start);

  while (distLabel[destination] < 0 && !vertexQueue.empty()){
    vertex = vertexQueue.front();

    vertexQueue.pop();
    if (distLabel[vertex] > distance)
      distance++;
    for (vector<int>::iterator
         it = Vertices[vertex].adjacencyList.begin();
         it != Vertices[vertex].adjacencyList.end(); it++)

      if (distLabel[*it] < 0)       {
        distLabel[*it] = distance + 1;
        predLabel[*it] = vertex;
        vertexQueue.push(*it);
      }
  }
  distance++;

  // Now reconstruct the shortest path if there is one
  vector<int> path(distance+1);
  if (distLabel[destination] < 0)
    cout << "Destination not reachable from start vertex\n";
  else   {
    path[distance] = destination;
    for (int k = distance - 1; k >= 0; k--)
      path[k] = predLabel[path[k+1]];
  }

  return path;
}

void Digraph::outputGraphviz() const {
  cout << "digraph D { \n";
  for (int i = 0; i < Vertices.size(); i++) {
    for (int j = 0; j < Vertices[i].adjacencyList.size(); j++)
      cout << i << "->" << Vertices[i].adjacencyList[j] << ";\n";
  }
  cout << "}" << endl;
}

int Digraph::getConnections()
{
	vector<bool> visited(Vertices.size(), false);
	int connections = 0;
	for(unsigned int start = 0; start < Vertices.size(); start++)
		checkConnections(start, visited, connections);
	return connections;
}

void Digraph::checkConnections(int start, vector<bool>& visited, int& amount)
{
	if(!visited[start]) amount++;

	visited[start] = true;

	for(vector<int>::iterator it = Vertices[start].adjacencyList.begin();
	it != Vertices[start].adjacencyList.end(); it++)
		if(!visited[*it]){
			visited[*it] = true;
			checkConnections(*it, visited, amount);
		};
}

unsigned int Digraph::getMaxDegree()
{
	if(Vertices.size() == 0) return -1;

	unsigned int max = Vertices[0].adjacencyList.size();

	for(unsigned int c = 1; c < Vertices.size(); c++)
		if(max < Vertices[c].adjacencyList.size())
			max = Vertices[c].adjacencyList.size();

	return max;
}

unsigned int Digraph::getMinDegree()
{
	if(Vertices.size() == 0) return -1;

	unsigned int min = Vertices[0].adjacencyList.size();

	for(unsigned int c = 1; c < Vertices.size(); c++)
		if(min > Vertices[c].adjacencyList.size())
			min = Vertices[c].adjacencyList.size();

	return min;
}
