/*--- Digraph.h ------------------------------------------------------------
                Header file for Digraph Class Template
 -------------------------------------------------------------------------*/

#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <list>
#include <vector>
#include <queue>
#include <iostream>
#include <fstream>
using namespace std;

class Digraph
{
 public:
  /***** Function Members *****/
  string getData(int k) const;
  /*-----------------------------------------------------------------------
    Retrieve data value in a given vertex.

    Precondition:  k is the number of a vertex.
    Postcondition: Data value stored in vertex k is returned.
   -----------------------------------------------------------------------*/

  void InsertNode(string st);
  void InsertEdge(int from, int to);

  void read(ifstream & inStream);
  /*-----------------------------------------------------------------------
    Input operation.

    Precondition:  ifstream inStream is open.  The lines in the file to
        which it is connected are organized so that the data item in a
        vertex is on one line and on the next line is the number of 
        vertices adjacent to it followed by a list of these vertices.
    Postcondition: The adjacency list representation of this digraph 
         has been stored in Vertices.
   -----------------------------------------------------------------------*/
	
	void read_matrix(ifstream & inStream);

 void display(ostream & out);
  /*-----------------------------------------------------------------------
    Output operation.

    Precondition:  ostream out is open.
    Postcondition: Each vertex and its adjacency list have
        been output to out.
   -----------------------------------------------------------------------*/

  void depthFirstSearch(int start);
  /*-----------------------------------------------------------------------
    Depth first search of digraph via depthFirstSearchAux(), starting
    at vertex start.

    Precondition:  start is a vertex.
    Postcondition: Digraph has been depth-first searched from start.
   -----------------------------------------------------------------------*/

  vector<int> shortestPath(int start, int destination);
  /*-----------------------------------------------------------------------
    Find a shortest path in the digraph from vertex start to vertex
    destination.

    Precondition:  start and destination are vertices.
    Postcondition: A vector of vertices along the shortest path from
        start to destination is returned.
   -----------------------------------------------------------------------*/
   void outputGraphviz() const;
   
   int getConnections(); //gets the connected components
   unsigned int getMaxDegree();
	unsigned int getMinDegree();

 private:
  /***** "Head nodes" of adjacency lists *****/
   class Vertex
   {
    public: 
     string data;
     vector<int> adjacencyList;
     Vertex() {};
     Vertex(string st) { data = st;}
   }; // end of Vertex class

  /***** Data Member *****/
   vector<Vertex> Vertices;

  /***** Private Function Member *****/
  void depthFirstSearch(int start, vector<bool> & unvisited);
  
  void checkConnections(int, vector<bool>&, int&);
  /*-----------------------------------------------------------------------
    Recursive depth first search of digraph, starting at vertex start.

    Precondition:  start is a vertex;  unvisited[i] is true if vertex i has
        not yet been visited, and is false otherwise.
    Postcondition: Vector unvisited has been updated.
   -----------------------------------------------------------------------*/

}; // end of Digraph class template declaration

#endif

