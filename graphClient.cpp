/*-------------------------------------------------------------------------
  Program to find the most direct route in an airline network from a given
  start city to a given destination city.  A digraph represented by its
  adjacency-list implementation is used for the network, and the
  information needed to construct it is read from networkFile.
 -------------------------------------------------------------------------*/
 
#include "Digraph.h"
#include <string>
#include <stdlib.h> 
using namespace std;


int main(int argc, char *argv[]) {
  if (argc < 3) { 
    cout << "Usage: " << argv[0] << " <Graph File>" << " <def or matrix> "
         << endl << "Note that def and matrix are different ways to make "
         << "the digraph." << endl;
    exit(1);
  }

 
  ifstream inFile(argv[1]);
  if (!inFile.is_open())
  {
    cerr << "*** Cannot open " << argv[1] << " ***\n";
    exit(-1);
  }

  Digraph d;
  string choice = argv[2];
  
	if(choice == "def")
		d.read(inFile);
	else if(choice == "matrix")
			d.read_matrix(inFile);
	else 
		return 1;
	
	
  cout << "The Digraph's ";
  d.display(cout);
  cout << endl;

  d.outputGraphviz();
  
  int start, destination;
  char response;
  do
  {
    cout << "Number of start city? ";
    cin >> start;
    cout << "Number of destination? ";
    cin >> destination;

    vector<int> path = d.shortestPath(start, destination);
    cout << "Shortest path is:\n";
    for (int k = 0; k < path.size()-1; k++)
    {
      cout <<  path[k] << " " << d.getData(path[k]) << endl;
      cout << "      |\n"
              "      v\n";
    }
    cout << destination << ' ' << d.getData(destination) << endl;
    cout << "\nMore (Y or N)?";
    cin >> response;
  }
  while (response == 'y' || response == 'Y');
  
  cout << endl << endl 
	   << "Connected components: " << d.getConnections() << endl
	   << "Max Degree: " << d.getMaxDegree() << endl
	   << "Min Degree: " << d.getMinDegree() << endl;
	
	return 0;
}
